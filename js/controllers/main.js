/**
 * Created by marc on 13.02.17. */
app.controller("AppController",
    function ($scope, $rootScope, $route, $q, $http, Restangular, Events, Analytics, Enrollments, Programs, DataElements, TrackedEntityAttributes, OneTEI, UserSettings, ProgramIndicators) {
    var mappedloaded = false;
    $scope.transferring = false;
    var removals = [];
    $scope.sections = {};
    $scope.sections.array = [false,false,false]
    $scope.sections.icons = ["+", "+", "+"]

    $scope.openSection = function(sectionNumber) {
        if ($scope.sections.array[sectionNumber]) {
            $scope.sections.array[sectionNumber]= false;
            $scope.sections.icons[sectionNumber] = "+";
        }
        else {
            $scope.sections.array[sectionNumber] = true;
            $scope.sections.icons[sectionNumber] = "-";
        }
    }

    $scope.openProgramSection = function(programId) {
        if ($scope.showPIFromPrograms[programId].value) {
            $scope.showPIFromPrograms[programId].value = false;
            $scope.showPIFromPrograms[programId].icon = "+";
        } else {
            $scope.showPIFromPrograms[programId].value = true;
            $scope.showPIFromPrograms[programId].icon = "-";
        }
    }
    
 
    $scope.$on('serverReady', function(){
        $scope.errorReport = {};
        $scope.errorReport.hasErrors = false;
        $scope.errorReport.array = [];
        console.log('Server ready for refresh');
        var systemSettings = Restangular.one("systemSettings");
        systemSettings.get().then (function (sys) {
            $scope.systemSettings = sys;
            $scope.HeaderTextSettings = $scope.systemSettings.applicationTitle;
        })
        var userSettings = Restangular.one("userDataStore", "flowApp").one("settings");
        userSettings.get().then(function (settings) {
            $scope.settings = {};
            $scope.settings.programLinks = settings.programLinks;
            $scope.settings.mappingAttributes = settings.mappingAttributes;
            $scope.settings.mappingProgramIndicators = settings.mappingProgramIndicators;
            $scope.settings.startDate = moment(settings.startDate);
            $scope.$emit('maintenance');
        }, function(res) {
            if (res.status == 404) {
                console.log("Creating new settings");
                newError("Warning!", "Warning","No settings available, creating the default settings");
                var settings = '{"programLinks":[],"mappingAttributes":{}, "mappingProgramIndicators":{},"startDate": "2017-01-01 00:00:00.000" }';
                userSettings.post('',settings).then(function(res) {
                    newError("Warning", "Settings created correclty");
                    $scope.$emit('serverReady');
                });
            } else {
                console.log('Error loading the settings');
                console.log(res);
                newError("Error", "Error loading the settings, see the browser logs for more information");
            }
        });  
    });

    $scope.$on('maintenance', function() {
        $scope.errorReport.hasErrorsInTransfer = false
        delete $scope.loadingCount;
        $scope.doingRefresh=true;
        $scope.doingRefresh = true;
        console.log(' Starting maintenance')
        var removals = [];

        if (!$scope.allEvents) $scope.allEvents = [];
        else $scope.allEvents.length = 0;

        if (!$scope.allNewEnrollments) $scope.allNewEnrollments = [];
        else $scope.allNewEnrollments.length = 0;

        if (!$scope.allEnrollments) $scope.allEnrollments = [];
        else $scope.allEnrollments.length = 0;

        if ($scope.settings.programLinks.length == 0) {
            newError("Warning", "No programs specified in settings");
        }
        if (angular.equals($scope.settings.mappingAttributes,{}))    {
            newError("Warning", "No mapping between tracked entity attributes and data elements specified in settings, the transfers will not include the attributes");
        }
        for (link of $scope.settings.programLinks) {
            var enrollmentsDefer = $q.defer();
            //var eventsDefer = $q.defer();
            removals.push(enrollmentsDefer.promise);
            //removals.push(eventsDefer.promise);

            Enrollments.customGETLIST('', {paging : false, ouMode: "ACCESSIBLE", program : link.trackerProgram.id, programStatus: "CANCELLED" })
            .then(function (enrollments) {
                enrollmentsDefer.resolve();
                var cancelledEnrollments = enrollments.length;
                for (en of enrollments.enrollments) {
                    var innerdeferred = $q.defer();
                    removals.push(innerdeferred.promise);
                    Restangular.one('events', en.enrollment).remove().then(innerdeferred.resolve());
                }
            });

            /*Function to delete all the events from the program that doesn't have any enrollment linked
            Would not be correct to use this function because then any directly entry on event capture would not be
            functional

            Events.customGETLIST('', {skipPaging : true, ouMode: "ACCESSIBLE", program: link.eventProgram.id})
            .then(function (events) {
                console.log('Events maintenance');
                eventsDefer.resolve();
                for (e of events.events) {
                    Restangular.one('enrollments', e.event).get().then(function(enrollment) {},function (res) {
                        var innerdeferred = $q.defer();
                        removals.push(innerdeferred.promise);
                        if (res.status == 404) Restangular.one('events', e.event).remove().then(innerdeferred.resolve);
                        else console.log('Error executing maintenance');
                    })
                }
            });
            */

        }

        $q.all(removals).then(function() {
            $scope.$emit('refreshData');
        });

    });



    
    $scope.$on('refreshData', function() {
        console.log('Refresh data from the server');
        var getPromises = [];

        for (link of $scope.settings.programLinks) {
            var activepromise = $q.defer();
            var completepromise = $q.defer();
            var eventsPromise = $q.defer();
            var piPromise = $q.defer();
            getPromises.push(piPromise.promise);
            getPromises.push(activepromise.promise);
            getPromises.push(completepromise.promise);
            getPromises.push(eventsPromise.promise);

            Enrollments.customGETLIST('', {paging : false, ouMode: "ACCESSIBLE", program : link.trackerProgram.id, programStatus: "ACTIVE" })
            .then(function (enrollments) {
                for (enrollment of enrollments.enrollments) {
                    var lastUpdatedDate = moment(enrollment.enrollmentDate);
                    $scope.allEnrollments.push(enrollment);
                    $scope.numberOfEn = $scope.allEnrollments.length;
                    if(lastUpdatedDate > $scope.settings.startDate) {
                        $scope.allNewEnrollments.push(enrollment);
                        $scope.numberOfNew = $scope.allNewEnrollments.length;
                    }
                }
                console.log("Active enrollments loaded");
                activepromise.resolve();
                $scope.$emit("loaded");
            });

            Enrollments.customGETLIST('', {paging : false, ouMode: "ACCESSIBLE", program : link.trackerProgram.id, programStatus: "COMPLETED" })
            .then(function (enrollments) {
                for (enrollment of enrollments.enrollments) {
                    var lastUpdatedDate = moment(enrollment.enrollmentDate);
                    $scope.allEnrollments.push(enrollment);
                    $scope.numberOfEn = $scope.allEnrollments.length;
                    if(lastUpdatedDate > $scope.settings.startDate) {
                        $scope.allNewEnrollments.push(enrollment);
                        $scope.numberOfNew = $scope.allNewEnrollments.length;
                    }
                }
                console.log("Completed enrollments loaded");
                completepromise.resolve();
                $scope.$emit("loaded");
            });

            Events.customGETLIST('', {skipPaging : true, ouMode: "ACCESSIBLE", program: link.eventProgram.id})
            .then(function (events) {
                for (event of events.events) {
                    $scope.allEvents.push(event);
                }
                console.log("Events loaded");
                eventsPromise.resolve();
                $scope.$emit("loaded");
            });
            

            ProgramIndicators.customGETLIST('', {paging : false, filter: "program.id:eq:"+link.trackerProgram.id, fields:"id,name,code,expression", order: "name:ASC"})
            .then(function (pis) {
                $scope.allProgramIndicators = {};
                $scope.allProgramIndicators[link.trackerProgram.id] = pis.programIndicators;
                console.log("Program indicators loaded");
                        console.log($scope.allProgramIndicators);
                piPromise.resolve();
            })

            $scope.showPIFromPrograms = {};
            $scope.showPIFromPrograms[link.trackerProgram.id] = {};
            $scope.showPIFromPrograms[link.trackerProgram.id].value = false;
            $scope.showPIFromPrograms[link.trackerProgram.id].icon = "+";


        }

        var programPromise = $q.defer();
        getPromises.push(programPromise.promise);

        Programs.customGETLIST('', {paging : false, fields: ":owner,!organisationUnits"}).then(function (programs) {
            $scope.allPrograms = {};
            $scope.allPrograms.array = programs.programs;
            addLookUp($scope.allPrograms);
            dividePrograms();
            console.log("Programs loaded");
            programPromise.resolve();
            $scope.$emit("loaded");
        });

        var teiPromise = $q.defer();
        getPromises.push(teiPromise.promise);

        TrackedEntityAttributes.customGETLIST('', {paging : false, fields:":owner"}).then(function (attributes) {
            $scope.allAttributes = attributes.trackedEntityAttributes;
            console.log("Tracked entity attributes loaded");
            teiPromise.resolve();
            $scope.$emit("mappedloaded");
        });

        
        var dePromise = $q.defer();
        getPromises.push(dePromise.promise);

        DataElements.customGETLIST('', {paging : false, fields: ":owner", filter: "domainType:eq:TRACKER"}).then(function (dataElements) {
            $scope.allIndividualDataElements = {};
            $scope.allIndividualDataElements.array = dataElements.dataElements;
            addLookUp($scope.allIndividualDataElements);
            console.log("Data elements loaded");
            dePromise.resolve();
            $scope.$emit("mappedloaded");

        });

        $q.all(getPromises).then(function() {
            loadMappings();
            newError("Success", "Refresh finished");
            $scope.doingRefresh = false;
            $scope.$evalAsync(
                function($scope) {
                    console.log('evalAsync');

                    $scope.numberOfNew = $scope.allNewEnrollments.length;
                    $scope.numberOfEn = $scope.allEnrollments.length;
                    $scope.numberOfEvents = $scope.allEvents.length;
                })
        });
    });

    var dividePrograms = function() {
        $scope.eventPrograms = [];
        $scope.trackerPrograms = [];
        for (var i = 0; i < $scope.allPrograms.array.length; i++) {
            if($scope.allPrograms.array[i].programType == "WITHOUT_REGISTRATION") $scope.eventPrograms.push($scope.allPrograms.array[i]);
            else $scope.trackerPrograms.push($scope.allPrograms.array[i]);
        }
    }

    var addLookUp = function(initialObject) {
        initialObject.lookUp = {};
        for (var i = 0; i < initialObject.array.length; i++) {
            initialObject.lookUp[initialObject.array[i].id] = initialObject.array[i];
        }
    }

    var loadMappings = function() {
        console.log("Loading mappings");
        for (var i = 0; i < $scope.allAttributes.length; i++) {
            if ($scope.settings.mappingAttributes[$scope.allAttributes[i].id]) {
                $scope.allAttributes[i].mappedDataElement = $scope.allIndividualDataElements.lookUp[$scope.settings.mappingAttributes[$scope.allAttributes[i].id]];
            }
        }

        for (link of $scope.settings.programLinks) {
            for (var i = 0; i < $scope.allProgramIndicators[link.trackerProgram.id].length; i++) {
                if ($scope.settings.mappingProgramIndicators[$scope.allProgramIndicators[link.trackerProgram.id][i].id]) {
                    $scope.allProgramIndicators[link.trackerProgram.id][i].mappedDataElement = $scope.allIndividualDataElements.lookUp[$scope.settings.mappingProgramIndicators[$scope.allProgramIndicators[link.trackerProgram.id][i].id]];
                }
            }
        }
    }

    $scope.newMapping = function (attribute) {
        $scope.settings.mappingAttributes[attribute.id] = attribute.mappedDataElement.id;
    }

    $scope.newPIMapping = function (pi) {
        $scope.settings.mappingProgramIndicators[pi.id] = pi.mappedDataElement.id;
    }



    $scope.startNewTransfer = function() {
        console.log("Starting new transfer");
        if ($scope.settings.programLinks.length == 0) {
            newError("Error!", "No programs specified in settings for the transfer");
        } else {
            var firstPromises = [];
            $scope.eventsToTransfer = [];
            var index = newError("Info!", "Starting new transfer");
            $scope.transferring = true;
            if ($scope.allEnrollments) {
                for (e of $scope.allEnrollments) {
                    var firstPromise = $q.defer();
                    firstPromises.push(firstPromise.promise);
                    var eventProgram = findEventProgram(e.program);
                    getEventsFromEnrollment(e, eventProgram, firstPromise);
                }
            } else {
                newError('Error!', 'No enrollments to transfer')
            }

            $q.all(firstPromises).then(function() {
                $scope.closeAlert(index);
                if (!$scope.errorReport.hasErrorsInTransfer) {
                    console.log('Transfer Done');
                    newError("Success!", "Transfer done successfully")
                    delete $scope.loadingCount;
                    $scope.tranferring = false;
                    $scope.settings.startDate = moment();
                    $scope.saveSettings();
                    $scope.$emit('maintenance');
                }
            });
        }
    }

    var findEventProgram = function(trackerProgramId) {
        var program;
        for (link of $scope.settings.programLinks) {
            if (link.trackerProgram.id == trackerProgramId) {
                program = link.eventProgram.id;
                break;
            }
        }

        return program;
    }

    var getEventsFromEnrollment = function(e, eventProgram, firstPromise) {
        console.log('Getting events from enrollment')
        var newEvent;
        $scope.teiInfo = [];
        Events.customGETLIST('', {skipPaging : true, orgUnit : e.orgUnit, program: e.program, trackedEntityInstance: e.trackedEntityInstance} )
            .then(function (events) {
                for (event of events.events) {
                    if (event.enrollment == e.enrollment) {
                        if (newEvent == undefined) {
                            newEvent = event;
                            delete newEvent.enrollment;
                            delete newEvent.enrollmentStatus;
                            delete newEvent.href;
                            delete newEvent.trackedEntityInstance;
                            newEvent.event = e.enrollment;
                            newEvent.eventDate = e.enrollmentDate;
                            newEvent.program = eventProgram;
                            newEvent.programStage = $scope.allPrograms.lookUp[eventProgram].programStages[0].id;
                        } else {
                            newEvent.dataValues = newEvent.dataValues.concat(event.dataValues);
                        }
                    }
                }
                getTrackedEntityInstances(newEvent, e, firstPromise);
            }, function(res) {
                $scope.errorReport.hasErrorsInTransfer = true;
                newError('Error!' ,"Bad response getting the events fom the API. See browser control for more information");
                console.log(res);
            });
    }

    var getTrackedEntityInstances = function(newEvent, e, firstPromise) {
        Restangular.one('trackedEntityInstances', e.trackedEntityInstance).get()
        .then(function(tei) {
            var values = [];
            for (a of tei.attributes) {
                var newDataValue = {};
                console.log(a.value);
                if ($scope.settings.mappingAttributes[a.attribute]) {
                    newDataValue.dataElement = $scope.settings.mappingAttributes[a.attribute];
                    newDataValue.value = a.value;
                    values.push(a.value);
                    newEvent.dataValues.push(newDataValue);
                }
            }
            //Add new function to capture the values of the program indicators and include them in the event to import.
            getAndPutProgramIndicatorsValues(newEvent, firstPromise);
        }, function(res) {
            $scope.errorReport.hasErrorsInTransfer = true;
            newError('Error!', 'Bad response getting the tracked entity instances fom the API. See browser control for more information' );
            console.log(res);
        });

    }

    var getAndPutProgramIndicatorsValues = function(newEvent, firstPromise) {
        var dateForPi = moment(newEvent.eventDate).format('YYYYMMDD');
        var piPromises = [];
        for (var pi in $scope.settings.mappingProgramIndicators) {
            var p = $q.defer();
            piPromises.push(p.promise);
            Analytics.customGETLIST('', {dimension: ["dx:" + pi, "pe:" + dateForPi], filter: "ou:" + newEvent.orgUnit})
            .then(function(res) {
                if (rows.length != 0) {
                    var newDataValue = {};
                    newDataValue.dataElement = $scope.settings.mappingProgramIndicators[pi];
                    newDataValue.value = res.rows[0][2];
                    newEvent.dataValues.push(newDataValue);
                }
                p.resolve();
            }, function(res) {
                $scope.errorReport.hasErrorsInTransfer = true;
                newError("Error", "Could not load the program indicator value");
                console.log(res);
            });
        }

        $q.all(piPromises).then(function() {
            $scope.eventsToTransfer.push(newEvent);
            postNewEventFromEnrollment(newEvent, firstPromise)
        });

    }

    var postNewEventFromEnrollment = function(newEvent, firstPromise) {
        Events.post(newEvent).then(function(response) {
            console.log('New event post done');
            firstPromise.resolve()
        }, function(response) {
            $scope.errorReport.hasErrorsInTransfer = true;
            newError('Error!', 'Error posting the new events, check the browser console for more information');
            console.log(response);
            firstPromise.resolve();
        });
    }


    $scope.removeProgramLink = function(i) {
        $scope.settings.programLinks.splice(i, 1);
    }

    $scope.addLink = function() {
        if ($scope.selectedTrackerProgram != undefined && $scope.selectedEventProgram != undefined) {
            var newLink = {};
            newLink.eventProgram = {};
            newLink.trackerProgram = {};
            newLink.eventProgram.name = $scope.selectedEventProgram.name;
            newLink.eventProgram.id = $scope.selectedEventProgram.id;

            newLink.trackerProgram.name = $scope.selectedTrackerProgram.name;
            newLink.trackerProgram.id = $scope.selectedTrackerProgram.id;

            $scope.settings.programLinks.push(newLink);
        }
    }

    $scope.saveSettings = function() {
        console.log("Saving settings");
        var index = newError("Info", "Saving new settings...");
        console.log("Index " + index);
        var userSettings = Restangular.one("userDataStore", "flowApp").one("settings");
        userSettings.programLinks = $scope.settings.programLinks;
        userSettings.mappingAttributes = $scope.settings.mappingAttributes;
        userSettings.mappingProgramIndicators = $scope.settings.mappingProgramIndicators;
        userSettings.startDate = $scope.settings.startDate;
        userSettings.put().then(function () {
            $scope.errorReport.array.splice(index,1);
            console.log('Settings saved');
            newError("Success", "Settings saved");
        }, function(res) {
            console.log(res);
            newError("Error!","Error saving settings, consult the browser console for more information");
        });        
    }

    var newError = function(header, info) {
        $scope.errorReport.hasErrors = true;
        var message = {};
        message.header = header;
        message.info = info;
        message.time = moment().format('hh:mm:ss a');
        var length = $scope.errorReport.array.push(message);
        return length-1;
    }

    $scope.closeAlert = function($index) {
        $scope.errorReport.array.splice($index, 1);
    }

    $scope.printProgramIndicators = function(index) {
        console.log("Index " + index);
        console.log($scope.allProgramIndicators);
        console.log(JSON.stringify($scope.allProgramIndicators[index]));
    }

    $scope.removeAttributeMapping = function(index)  {
        console.log("Removing attribute mapping");
        delete $scope.settings.mappingAttributes[$scope.allAttributes[index].id];
        delete $scope.allAttributes[index].mappedDataElement;
    }

    $scope.removePIMapping = function(index, programId)  {
        console.log("Removing program indicator mapping");
        delete $scope.settings.mappingProgramIndicators[$scope.allProgramIndicators[programId][index].id];
        delete $scope.allProgramIndicators[programId][index].mappedDataElement;
    }

});

