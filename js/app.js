/**
 * Created by marc on 10.02.17.
 */
'use strict';

/**
 * Declare app level module.
 */
var app = angular.module('App', [
    'ngResource',
    'ngRoute',
    'ui.bootstrap',
    'restangular',
    'ngTouch'
]).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/main', {
        templateUrl: 'partials/main.html', controller: 'AppController'
    });
    $routeProvider.when('/settings', {
        templateUrl: 'partials/settings.html', controller: 'AppController'
    });
    $routeProvider.otherwise({redirectTo: '/main'});
}]);

app.run(function ($http, $rootScope, Restangular) {
    /**
     * Get the location path of the manifest
     *
     * @returns {string} URI of the manifest.webapp
     */
    function getManifestPath() {
        var path = window.location.pathname.split('/');
        path.pop(); //Removes index.html
        path = path.join('/');
        //change to http for localhost test
        //change to https for server test
        return 'https://' + window.location.host + path + '/manifest.webapp';
    } 

    function getSettingsPath() {
        var path = window.location.pathname.split('/');
        path.pop(); //Removes index.html
        path = path.join('/');
        return 'https://' + window.location.host + path + '/settings.json';
    }

    function initRestangular(path) {
        console.log(path + '/api/27');
        Restangular.setBaseUrl(path + '/api/27');
        Restangular.setDefaultHttpFields({fields: ':owner'});
        Restangular.setExtraFields([':all']);
        $rootScope.$broadcast('serverReady');
    }

    /**
     * Make a http call to the server to retrieve the manifest and
     * save it to the angular root scope.
     */
    $http.get(getManifestPath()).success(function (manifest) {
        $rootScope.manifest = manifest;
        window.instance = manifest.instance;
        initRestangular(manifest.activities.dhis.href);
      
    }).error(function () {
        $rootScope.manifest = [];
    });
});

app.directive('ngConfirmClick', [
        function(){
            return {
                link: function (scope, element, attr) {
                    var msg = attr.ngConfirmClick || "Are you sure?";
                    var clickAction = attr.confirmedClick;
                    element.bind('click',function (event) {
                        if ( window.confirm(msg) ) {
                            scope.$eval(clickAction)
                        }
                    });
                }
            };
    }]);


