/**
 * Created by marc on 13.02.17.
 */
'use strict';

app.factory('Events', function (Restangular) {
        return Restangular.all('events');
    })
    .factory('OneTEI', function (Restangular) {
        return Restangular.all('trackedEntityInstances');
    })
    .factory('Programs', function (Restangular) {
        return Restangular.all('programs');
    })
    .factory('TrackedEntityAttributes', function(Restangular) {
    	return Restangular.all('trackedEntityAttributes');
    })
    .factory('DataElements', function(Restangular) {
    	return Restangular.all('dataElements');
    })
    .factory('Enrollments', function(Restangular) {
    	return Restangular.all('enrollments');
    })
    .factory('UserSettings', function(Restangular) {
    	return Restangular.all('userDataStore');
    })
    .factory('ProgramIndicators', function(Restangular) {
        return Restangular.all('programIndicators');
    })
    .factory('Analytics', function(Restangular) {
        return Restangular.all('analytics');
    })